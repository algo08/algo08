/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.algo8;

import java.util.Scanner;

/**
 *
 * @author BankMMT
 */
// [1,2,3,4]       <--- n = size of array <--- 4
// b = 
// [1,2,3,4]
// b = 1           <--- b change to index 0 of array <--- b = backup
// i = 0
// [2,3,4,4]       <--- for (int j=0 ; j<n ; j++) 
// b = 1                   if (i<(n-i)-1) <--- (0 < (4-0)-1)  <--- 0 1 2
// i = 0                       a[i] = a[i+1];
// [2,3,4,1]       <--- index n-(i+1) change value to b <--- index 4-0 change value to 1
// b = 1
// i = 0
// [2,3,4,1]       <--- loop process and i++ but less than array size <--- (i < n)
// b = 2
// i = 1
// [3,4,4,1]       <---  for (int j=0 ; j<n ; j++) 
// b = 2                    if (i<n-1) <--- (0 < (4-1)-1)  <--- 0 1;
// i = 1                        a[i] = a[i+1]
// [3,4,2,1]       <--- index n-(i+1) change value to b <--- index 4-2 change value to 2
// b = 2
// i = 1
// [3,4,2,1]       <--- loop process and i++ but less than array size <--- (i < n)
// b = 3 
// i = 2
// [4,4,2,1]       <--- for (int j=0 ; j<n ; j++) 
// b = 3                   if (i<(n-i)-1) <--- (0 < (4-2)-1)  <--- 0 
// i = 2                       a[i] = a[i+1];
// [4,3,2,1]       <--- index n-(i+1) change value to b <--- index 4-2 change value to 2
// b = 3
// i = 2           
// [4,3,2,1]       <--- this is answer!! and last process is swap between index 0 and 1
//                     it reduce 1 loop count so ---> for (int i=0 ; i<a.length-1 ; i++)
public class Algo08 {

    public static void main(String[] args) {
        Scanner pm = new Scanner(System.in);
        System.out.println("Enter size of array : ");
        int n = pm.nextInt();
        int a[] = new int[n];
        int backup;
        for (int i = 0; i < a.length; i++) {
            a[i] = pm.nextInt();
        }
        for (int i = 0; i < a.length; i++) {
            backup = a[0];
            for (int j = 0; j < a.length; j++) {
                if (j < (n - i) - 1) {
                    a[j] = a[j + 1];
                }

                // for (int k=0 ; k<n ; k++){
                //  System.out.print(a[k]+ " ");
                // }System.out.println();
            } 
            a[n - (i + 1)] = backup;
            //System.out.println(" back up = " + backup);
        }
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
    }
}
